Making an interpreter in Go.

Following https://interpreterbook.com/


All code belongs to Thorsten Bell as it's written directly from the book.

`go run main.go`  or `go test ./lexer`
